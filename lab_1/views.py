from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = "Nandhika Prayoga"
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 11, 20) #TODO Implement this, format (Year, Month, Date)
npm = 1706039912 # TODO Implement this
college = "Universitas Indonesia"
hobbies = "I do like read and write any book, love to learn something new, and do creative thing."
description = "I'm a dreamer."
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 
    	'npm': npm, 'college': college, 'hobbies': hobbies, 'description': description}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
